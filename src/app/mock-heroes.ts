import {Hero} from './hero';

export const HEROES: Hero[] = [
  {id: 11, name: 'Mr. Nice'},
  {id: 12, name: 'John weak'},
  {id: 13, name: 'Supper man'},
  {id: 14, name: 'Spider man'},
  {id: 15, name: 'Hulk'},
  {id: 16, name: 'Iron man'},
  {id: 17, name: 'Carl breek'},
  {id: 18, name: 'Zahidul Islam'},
  {id: 19, name: 'Mack will'},
  {id: 20, name: 'Imran Hasan'}
];
